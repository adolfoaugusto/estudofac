<html>
  <p>
    <?php
    // Obtenha uma string parcial a partir do seu nome
    $nome = "Adolfo";
    $partial = substr($nome, 0, 3);
    // e a imprima na tela!
    echo $partial;
    ?>
  </p>
  <p>
    <?php
    // Coloque seu nome em letras maiúsculas e imprima-o na tela:
    $uppercase = strtoupper($nome);
    echo $uppercase;
    ?>
  </p>
  <p>
    <?php
    // Coloque seu nome em letras minúsculas e imprima-o na tela:
    $lowercase = strtolower($uppercase);
    print $lowercase;
    ?>
  </p>
</html>