<html>
    <p>
<?php
    // Use rand() para imprimir um número aleatório na tela
    print rand();
?>
    </p>
    <p>
<?php
    // Use seus conhecimentos em strlen(), substr(), e rand() para
    // imprimir um caractere aleatório do seu nome na tela.
    $name = "adolfo";
    $tes = strlen($name);
    $res = rand(0, $tes - 1);
    print substr($name, $res, 1);
?>
    </p>
</html>