<html>
    <p>
    <?php
    // Tente arredondar um número em ponto flutuante para um inteiro
    // e imprima-o na tela
     $round = round(M_PI); 
    print $round;
    ?>
    </p>
    <p>
    <?php
    // Tente arredondar um número em ponto flutuante para 3 casas decmais
    // e imprima-o na tela
    $round_decimal = round(M_PI, 3);
    echo $round_decimal;
    ?>
    </p>
</html>